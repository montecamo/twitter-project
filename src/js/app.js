import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

import '../css/style.css';
import 'bootstrap/dist/css/bootstrap.css';
import store from './store.js';
import Router from './router.js';

library.add(faEdit);
library.add(faTrashAlt);

render((
  <Provider store={store}>
    <Router />
  </Provider>
), document.getElementById('root'));
