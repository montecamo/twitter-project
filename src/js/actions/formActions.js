export function onChange(content) {
  return (dispatch, getState) => {
    let { maxLen } = getState().form;
    let len = content.length;

    if (len > maxLen) {
      return dispatch({
        type: 'MAX_TWEET_LENGTH',
        payload: true
      });
    }

    if (getState().alerts.maxTweetLength) {
      dispatch({
        type: 'MAX_TWEET_LENGTH',
        payload: false
      })
    };

    dispatch({
      type: 'HANDLE_CHANGE',
      payload: content 
    });
  }
}

export function scrollForm(status) {
  return {
    type: 'SCROLL_FORM',
    payload: status 
  }
}

export function focusForm(status) {
  return {
    type: 'FOCUS_FORM',
    payload: status
  }
}

export function startEdit(name) {
  return {
    type: 'START_EDIT',
    payload: name 
  }
}

export function stopEdit() {
  return {
    type: 'STOP_EDIT',
  }
}

export function clearForm() {
  return dispatch => {
    dispatch(onChange(''));
  }
}

export function editForm(tweet) {
  return dispatch => {
    dispatch(onChange(tweet.content));
    dispatch(startEdit(tweet.name));
  }
}

export function setTempName() {
  let tempName = Math.random().toString(32).substring(2, 15);

  return {
    type: 'SET_TEMPNAME',
    payload: tempName
  }
}
