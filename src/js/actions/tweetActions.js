import axios from 'axios';
import { authRef } from '../firebase';

export function addTweet() {
  return (dispatch, getState) => {
    let { name, content } = getState().form;
    console.log('TWEET', { name, content });
    if (!content) return;
  
    dispatch({
      type: 'ADD_TWEET',
      payload: { name, content } 
    });
  }
}

export function tweetDeleted(name) {
  return {
    type: 'TWEET_DELETED',
    name
  }
}

export function deleteTweet(name) {
  return (dispatch, getState) => {
    let { uid } = getState().user.data;

    axios.delete(`https://twitter-project-c0405.firebaseio.com/users/${uid}/tweets/${name}.json`)
    .then((res) => {
      console.log(res);
      dispatch(tweetDeleted(name));
    })
    .catch((err) => {
      console.log(err);
    })
  }
}

export function updateName(prevName, nextName) {
  return {
    type: 'UPDATE_NAME',
    payload: {prevName, nextName}
  }
}

export function dispatchTweet(tempName) {
  return (dispatch, getState) => {
    let { user, form } = getState();
    let { name: tempName, content } = form;
    console.log('DISPATCH', name, content);
    if (!content) return;

    axios.post(`https://twitter-project-c0405.firebaseio.com/users/${user.data.uid}/tweets.json`, {
        content
      }) 
      .then((res) => {
        console.log('add tweet res', res);
        dispatch(updateName(tempName, res.data.name))
      })
      .catch((err) => {
        console.log('add tweet err', err);
      })
  }
}


export function dispatchUpdatedTweet() {
  return (dispatch, getState) => {
    let { user, form } = getState();
    let { name, content } = form;
    if (!content) return;

    axios.patch(`https://twitter-project-c0405.firebaseio.com/users/${user.data.uid}/tweets/${name}.json`, {
      content
    })
    .then((res) => {
      console.log('update tweet res', res);
    })
    .catch((err) => {
      console.log('update tweet err', err);
    })
  }
}

export function fetchTweetsSuccess(tweets) {
  return {
    type: 'RECEIVE_TWEETS',
    payload: tweets
  }
}

export function clearCurrTweet() {
  return {
    type: 'CLEAR_TWEET'
  }
}

export function updateTweet(tweet) {
  return (dispatch, getState) => {
    let { name, content } = getState().form;
    console.log('TWEET', { name, content });
    if (!content) return;

    dispatch({
      type: 'UPDATE_TWEET',
      payload: { name, content } 
    });
  }
}

export function fetchTweets(userId) {
  return (dispatch, getState) => {
    let { user } = getState();
    let id = user.data ? user.data.uid : userId;
    axios.get(`https://twitter-project-c0405.firebaseio.com/users/${id}/tweets.json`)
      .then((res) => {
        console.log(res);
        return res.data;
      })
      .then((tweets) => {
        let converted = [];
        for (let key in tweets) {
          if (!tweets.hasOwnProperty(key)) continue;
          let tweet = tweets[key];
          tweet.name = key;
          converted.push(tweet)
        }
        console.log(converted);
        return converted;
      })
      .then((convertedTweets) => {
        dispatch(fetchTweetsSuccess(convertedTweets));
      })
      .catch((err) => {
        console.log('fetch tweets err', err);
      })
  }
}
