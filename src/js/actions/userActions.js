import axios from 'axios';
import { passUpdate, emailUpdate } from './alertActions';
import { authRef } from '../firebase';
import { push, goBack } from 'connected-react-router';

export function signOut() {
  return dispatch => {
    authRef
      .signOut()
      .then(() => {
        dispatch({
          type: 'SIGNED_OUT'
        })
      })
      .catch((err) => {
        console.log(err);
      })
  }
}

export function signIn(user) {
  let { email, password } = user;
  return dispatch => {
    authRef
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        dispatch(fetchUser())
      })
      .catch((err) => {
        console.log(err);
      })
  }
}

export function signUp(user) {
  let { email, password } = user;
  return dispatch => {
    authRef
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        dispatch(fetchUser());
        dispatch(dispatchUser(data.user));
      })
      .catch((err) => {
        console.log(err);
      })
  }
}

export function updateEmail(newEmail) {
  return (dispatch) => {
    authRef.currentUser.updateEmail(newEmail)
    .then(() => {
      dispatch(fetchUser());
      console.log('before email update success');
      dispatch(emailUpdate(true));
    })
    .catch((err) => {
      console.log(err);
      dispatch(emailUpdate(false));
    })
  }
}

export function updatePass(newPass) {
  return (dispatch) => {
    authRef.currentUser.updatePassword(newPass)
    .then(() => {
      dispatch(fetchUser());
      dispatch(passUpdate(true));
    })
    .catch((err) => {
      console.log(err);
      dispatch(passUpdate(false));
    })
  }
}

export function fetchUser() {
  console.log('fetch user');
  return (dispatch, getState) => {
    authRef.onAuthStateChanged(user => {
      let reduxUser = getState().user;
      if (user) {
        console.log(user);
        dispatch({
          type: 'FETCH_USER',
          payload: user 
        });
        if (reduxUser.authRedirectPromise) {
          reduxUser.authRedirectPromise();
          dispatch(deletePromise());
        }
      } else {
        dispatch({
          type: 'FETCH_USER',
          payload: null
        })
      }
    })
  }
}

export function dispatchUser(user) {
  return (dispatch, getState) => {
    authRef.currentUser.getIdToken()
    .then((token) => {
      axios.put(`https://twitter-project-c0405.firebaseio.com/users/${user.uid}.json?auth=${token}`,{
        email: user.email
      })
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      })
    })
  }
}

export function addPromise(promise) {
  return {
    type: 'ADD_PROMISE',
    payload: promise 
  }
}

export function deletePromise() {
  return {
    type: 'DELETE_PROMISE',
  }
}

export function authRequired() {
  return (dispatch, getState) => {
    dispatch(addPromise(() => {
      dispatch(goBack())
    }));
  }
}
