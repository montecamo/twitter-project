export function update(dispatch) {
  let { type, success } = this;

  setTimeout(() => {
    dispatch({
      type: type,
      payload: {
        expired: true
      }
    })
  }, 1500);

  dispatch({
    type: type,
    payload: {
      expired: false,
      status: success ? 'success' : 'failed'
    } 
  });
}

export function emailUpdate(success) {
  return update.bind({ success, type: 'EMAIL_UPDATE' });
}

export function passUpdate(success) {
  return update.bind({ success, type: 'PASS_UPDATE' });
}
