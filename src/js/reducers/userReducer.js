let initialState = {
  data: {},
  authorized: false,
  authRedirectPromise: null
}

export default function userReducer(state=initialState, action) {
  switch(action.type) {
  case 'FETCH_USER': {
    state = {...state, data: {...action.payload}, authorized: !!action.payload}; 
    break;
  }
  case 'SIGNED_OUT': {
    state = {...state, data: null, authorized: false};
    break;
  }
  case 'ADD_PROMISE': {
    state = {...state, authRedirectPromise: action.payload};
    break;
  }
  case 'DELETE_PROMISE': {
    state = {...state, authRedirectPromise: null};
    break;
  }
  }
  return state;
}
