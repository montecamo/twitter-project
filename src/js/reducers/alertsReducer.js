let initialState = {
  passUpdate: false,
  emailUpdate: false,
  maxTweetLength: false
}

const alertsReducer = (state=initialState, action) => {
  console.log(action.type, action.payload);
  switch(action.type) {
  case 'EMAIL_UPDATE': {
    let info = action.payload;
    state = {...state, emailUpdate: !info.expired && info.status};
    break;
  }
  case 'PASS_UPDATE': {
    let info = action.payload;
    state = {...state, passUpdate: !info.expired && info.status};
    break;
  }
  case 'MAX_TWEET_LENGTH': 
    state = {...state, maxTweetLength: action.payload} 
  }
  return state;
} 

export default alertsReducer;
