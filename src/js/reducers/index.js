import { combineReducers } from 'redux';

import tweetsReducer from './tweetsReducer';
import formReducer from './formReducer';
import userReducer from './userReducer';
import alertsReducer from './alertsReducer';

let reducer = combineReducers({
  form: formReducer,
  tweets: tweetsReducer,
  user: userReducer,
  alerts: alertsReducer
});

export default reducer;
