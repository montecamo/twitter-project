localStorage.setItem('tweets', JSON.stringify([]));
let initialState = {
  data: JSON.parse(localStorage.getItem('tweets')),
  currTweet: {}
}

export default function tweetsReducer(state=initialState, action) {
  let tweets = state.data.concat();
  switch(action.type) {
  case 'ADD_TWEET': {
    let { name, content } = action.payload;

    tweets.push({ name, content });
    localStorage.setItem('tweets', JSON.stringify(tweets));
    state = {...state, data: tweets}
    break;
  }
  case 'TWEET_DELETED':
    tweets = tweets.filter((tweet) => {
      return tweet.name !== action.name;
    });
    localStorage.setItem('tweets', JSON.stringify(tweets));
    state = {...state, data: tweets};
    break;
  case 'RECEIVE_TWEETS': 
    tweets = action.payload;
    localStorage.setItem('tweets', JSON.stringify(tweets));
    state = {...state, data: tweets}
    break;
  case 'UPDATE_NAME': 
    let { prevName, nextName } = action.payload;
    tweets.some((tweet) => {
      if (tweet.name === prevName) {
        tweet.name = nextName;
        return true;
      }
      return false;
    });
    localStorage.setItem('tweets', JSON.stringify(tweets));
    state = {...state, data: tweets}
    break;
  case 'UPDATE_TWEET': {
    let { name, content } = action.payload;
    tweets.some((tweet) => {
      if (tweet.name === name) {
        tweet.content = content;
        return true;
      }
      return false;
    })
    localStorage.setItem('tweets', JSON.stringify(tweets));
    state = {...state, data: tweets}
 
    break;
  }
  case 'HANDLE_CHANGE': 
    state = {...state, currTweet: {...state.currTweet, content: action.payload}};
    console.log('updated curr tweet', state.currTweet);
    break;
  case 'START_EDIT':
    state = {...state, currTweet: {...state.currTweet, name: action.payload}};
  case 'CLEAR_TWEET':
    state = {...state, currTweet: {}};
  }
  return state;
}
