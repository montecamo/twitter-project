let initialState = {
  content: '',
  edit: false,
  name: null,
  focus: false,
  scroll: false,
  letters: 0,
  maxLen: 140 
}

const formReducer = (state=initialState, action) => {
  switch(action.type) {
    case 'HANDLE_CHANGE': 
      let content = action.payload;
      state = {...state, content, letters: content.length};
      break;
    case 'SCROLL_FORM': 
      state = {...state, scroll: action.payload};
      break;
    case 'FOCUS_FORM': 
      state = {...state, focus: action.payload};
      break;
    case 'START_EDIT': 
      state = {...state, edit: true, name: action.payload};
      break;
    case 'STOP_EDIT': 
      state = {...state, edit: false, name: null};
      break;
    case 'SET_TEMPNAME':
      state = {...state, name: action.payload};
      break;
  }
  return state;
}

export default formReducer;
