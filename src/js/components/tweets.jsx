import React from 'react';

import TweetsList from './tweets-list.jsx';
import Form from './form.jsx';


let Tweets = (props) => (
  <div className='tweets'>
    {props.type === 'home' && <Form/>}
    <TweetsList {...props} type={props.type} />
  </div>
);

export default Tweets;
