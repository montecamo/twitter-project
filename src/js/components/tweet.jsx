import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroupItem } from 'reactstrap';

import { scrollForm, focusForm, startEdit, onChange } from '../actions/formActions';
import { deleteTweet } from '../actions/tweetActions';

@connect(null, mapDispatchToProps)
export default class Tweet extends Component {
  constructor() {
    super();
    this.state = {
      active: false,
    }
    this.toggle = this.toggle.bind(this);
    this.deleteTweet = this.deleteTweet.bind(this);
  }

  toggle(e) {
    this.setState((prevState) => {
      if (!prevState.active) {
        setTimeout(() => {
          window.addEventListener('click', this.toggle, {once: true});
        }, 0);
        return { active: true };
      } else {
        return { active: false };
      }
    });
  }

  deleteTweet() {
    this.props.deleteTweet(this.props.name);
  }

  render() {
    let { active } = this.state;
    return (
      <ListGroupItem onClick={this.toggle} className={'tweet ' + (active && 'tweet-hover')} >
        <FontAwesomeIcon onClick={this.props.editTweet} icon='edit'/>
        {'  ' + this.props.content}
        <div className={'delete ' + (active && 'tweet-hover_delete')}>
          <FontAwesomeIcon 
            icon='trash-alt'
            onClick={this.deleteTweet}
            className={'delete-icon ' + (active && 'tweet-hover_delete-icon')} />
        </div>
      </ListGroupItem>
    )
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    editTweet: (e) => {
      e.stopPropagation();
      let { name, content } = ownProps;
      dispatch(onChange(content));
      dispatch(startEdit(name));
      dispatch(scrollForm(true));
      dispatch(focusForm(window.scrollY / 2));
    },
    deleteTweet: (name) => {
      dispatch(deleteTweet(name));
    } 
  }
}
