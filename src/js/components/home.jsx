import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import Tweets from './tweets.jsx';
import Greeting from './greeting.jsx';


const Home = ({ user }) => (
  <div className='wrapper'>
    {user.authorized && <Tweets type='home' />} 
    {!user.authorized && <Greeting />}
  </div>
);

const mapStateToProps = ({ user }) => ({user});

export default connect(mapStateToProps)(Home);
