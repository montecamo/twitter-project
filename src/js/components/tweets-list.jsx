import React, { Component } from 'react';
import { ListGroup } from 'reactstrap';
import { connect } from 'react-redux';

import { fetchTweets } from '../actions/tweetActions';
import Tweet from './tweet.jsx';


@connect(mapStateToProps, mapDispatchToProps)
export default class TweetsList extends Component {
  componentDidMount() {
    if (this.props.type === 'home') {
      this.props.fetchTweets();
    } else if (this.props.type === 'custom') {
      this.props.fetchTweets(this.props.match.params.id);
    }
  }

  render() {
    return (
      <ListGroup className='list'>
        {renderTweets(this.props.tweets)}
      </ListGroup>
    )
 }
}

function renderTweets(tweets) {
  return tweets.map((tweet, index) => (
    <Tweet key={index} content={tweet.content} name={tweet.name}/>
  ))
}

function mapStateToProps({tweets, user}) {
  return {
    tweets: tweets.data,
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchTweets: (id) => {
      dispatch(fetchTweets(id))
    }
  }
}
