import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, FormFeedback } from 'reactstrap';
import TextAreaAutosize from 'react-autosize-textarea';

import {addTweet, dispatchTweet, updateTweet, dispatchUpdatedTweet} from '../actions/tweetActions';
import {onChange, clearForm, stopEdit, scrollForm, focusForm, setTempName} from '../actions/formActions';
import LetterCounter from './letter-counter.jsx';


@connect(mapStateToProps, mapDispatchToProps)
export default class Form extends Component { 
  constructor() {
    super();

    this.scroll = this.scroll.bind(this);
    this.focus = this.focus.bind(this); 
  }

  componentDidUpdate() {
    let { scroll, focus } = this.props;

    if (scroll) {
      this.scroll();
      this.props.scrollDone();
    }

    if (focus || typeof focus === 'number') {
      this.focus(focus);
      this.props.focusDone();
    }
  }

  scroll() {
    console.log('scrolling...');
    setTimeout(() => {
      this.textarea.scrollIntoView({ behavior: 'smooth' });
    },0);
  }

  focus(timeout) {
    console.log('focusing...');
    if (typeof timeout === 'number') {
      return setTimeout(() => {
        this.textarea.focus();
      }, timeout)
    }

    this.textarea.focus();
  }

  render() {
    let { edit, content, handleChange,
          addTweet, updateTweet, cancelUpdate, alerts } = this.props;
    console.log
    return (
      <div className='form'>
        <TextAreaAutosize 
          className={'form-control ' + (alerts.maxTweetLength ? 'is-invalid' : '')}
          innerRef={textarea => this.textarea = textarea}
          rows={3}
          value={content} 
          onChange={handleChange}/>
        <LetterCounter />
        <div className='btn-wrapper'> 
          {!edit && <Button 
            className='add-btn'
            color='danger' 
            onClick={addTweet}>Add</Button>}
          {edit && <Button 
            className='add-btn save'
            color='danger' 
            onClick={updateTweet}>Save</Button>}
          {edit && <Button 
            className='add-btn'
            color='danger' 
            onClick={cancelUpdate}>Cancel</Button>}
       </div>
      </div> 
    )
  }
}

function mapStateToProps({form, alerts}) {
  return {...form, alerts };
}

function mapDispatchToProps(dispatch) {
 return {
    addTweet: () => {
      dispatch(setTempName());
      dispatch(addTweet());
      dispatch(dispatchTweet());
      dispatch(clearForm());
    },
    updateTweet: () => {
      dispatch(updateTweet());
      dispatch(dispatchUpdatedTweet());
      dispatch(stopEdit());
      dispatch(clearForm());
    },
    cancelUpdate: () => {
      dispatch(clearForm());
      dispatch(stopEdit());
    },
    handleChange: (e) => {
      dispatch(onChange(e.target.value))
    },
    scrollDone: () => {
      dispatch(scrollForm(false));
    },
    focusDone: () => {
      dispatch(focusForm(false));
    }
  }
}
