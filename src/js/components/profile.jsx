import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { 
  Form,
  FormGroup,
  FormFeedback,
  Label,
  Input,
  Button
} from 'reactstrap'

import { updateEmail, updatePass, authRequired } from '../actions/userActions';
import { emailUpdate, passUpdate } from '../actions/alertActions';


@connect(mapStateToProps, mapDispatchToProps)
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    let {email, password} = this.state;
    
    if (email) {
      this.props.updateEmail(email);
      this.setState({ email: '' });
    }

    if (password) {
      this.props.updatePass(password);
      this.setState({ password: '' });
    }  
  }

  handleChange(e) {
    let { id, value } = e.target;
    
    switch(id) {
    case 'email':
      return this.setState({ email: value });
    case 'password':
      return this.setState({ password: value });
    }
  }

  render() {
    let { user, alerts } = this.props;
    let { emailUpdate, passUpdate } = alerts;

    return (
      <div className='wrapper'>
        <Form onSubmit={this.handleSubmit} className='auth-form'>
          <FormGroup>
            <Label for='email'>New email</Label>

            <Input 
              type='email'
              name='email'
              id='email'
              placeholder={user.data.email}
              value={this.state.email}
              autoComplete='off'
              valid={emailUpdate === 'success'}
              invalid={emailUpdate === 'failed'}
              onChange={this.handleChange} />

            <FormFeedback tooltip>Email update failed</FormFeedback>
            <FormFeedback valid tooltip>Email update successful</FormFeedback>
          </FormGroup> 
          <FormGroup>
            <Label for='password'>New password</Label>

            <Input
              type='password'
              name='password'
              id='password'
              valid={passUpdate === 'success'}
              invalid={passUpdate === 'failed'}
              value={this.state.password}
              onChange={this.handleChange} />

            <FormFeedback tooltip>Password update failed</FormFeedback>
            <FormFeedback valid tooltip>Password update successful</FormFeedback>
          </FormGroup>
          <Button>Save</Button>
        </Form>
      </div>
    )
  }
}

function mapStateToProps({ user, alerts }) {
  return { user, alerts }
};

function mapDispatchToProps(dispatch) {
  return {
    updateEmail: (newEmail) => {
      dispatch(updateEmail(newEmail));
    },
    updatePass: (newPass) => {
      dispatch(updatePass(newPass));
    },
  }
}
