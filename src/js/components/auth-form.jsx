import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Form, FormText, Button, FormGroup, Label, Input } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';

import { signUp, signIn } from '../actions/userActions';


@connect(mapStateToProps, mapDispatchToProps)
export default class AuthForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      type: props.type 
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    let inp = e.target;

    switch(inp.id) {
      case 'email':
        this.setState({ email: inp.value });
        break;
      case 'password':
        this.setState({ password: inp.value });
        break;
    }
  }

  componentDidUpdate() {
    let { type } = this.props;

    if (this.state.type !== type) {
      this.setState({
        email: '',
        password: '',
        type: type
      })
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    let { type } = this.props;
    let user = {
      email: this.state.email,
      password: this.state.password
    }
    
    if (type === 'login') {
      this.props.signIn(user)
    } else {
      this.props.signUp(user);
    }
  }

  render() {
    let { type, user } = this.props;

    return (
      <div className='wrapper'>
        <Form onSubmit={this.handleSubmit} className='auth-form'>
          <FormGroup>
            <Label for='email'>Email</Label>
            <Input 
              type='email'
              name='email'
              id='email'
              value={this.state.email}
              onChange={this.handleChange} />
          </FormGroup> 
          <FormGroup>
            <Label for='password'>Password</Label>
            <Input
              type='password'
              name='password'
              id='password'
              value={this.state.password}
              onChange={this.handleChange} />
          </FormGroup>
          <Button>{type === 'login' ? 'Login' : 'Register'}</Button>
          {type === 'login' && 
            <FormText>
              <Link className='form-link' to='/signup'>Registration</Link>
            </FormText>}
        </Form>
      </div>
    )
  }
}

function mapStateToProps({ user }) {
  return {user}
}

function mapDispatchToProps(dispatch) {
  return {
    signIn: (user) => {
      dispatch(signIn(user))
    },
    signUp: (user) => {
      dispatch(signUp(user))
    }
  }
}
