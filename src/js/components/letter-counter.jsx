import React, { Component } from 'react';
import { connect } from 'react-redux';


const LetterCounter = (props) => {
  let { counter, danger, max } = props;
  
  return (
    <div className={'letter-counter' + (danger ? ' danger' : '')}>
      {`${counter}/${max}`}
    </div>
  )
}

const mapStateToProps = ({ form, alerts }) => {
  return {
    counter: form.letters,
    max: form.maxLen,
    danger: alerts.maxTweetLength 
  }
}

export default connect(mapStateToProps)(LetterCounter);
