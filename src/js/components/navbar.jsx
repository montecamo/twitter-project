import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { 
  Collapse,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  Button
} from 'reactstrap';

import { signOut } from '../actions/userActions';
import logo from '../../images/logo.svg';


@connect(mapStateToProps, mapDispatchToProps)
export default class NavBar extends Component {
  constructor() {
    super();
    this.toggle = this.toggle.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      isOpen: false
    };
  }

  handleClick() {
    if (this.state.isOpen) {
      this.setState({ isOpen: false });
    }
  }

  toggle() {
    this.setState((state) => {
      return {
        isOpen: !state.isOpen
      }
    });
  }

  render() {
    let { user } = this.props;
    return (
      <Navbar color="light" light expand="sm">
        <Link to='/' onClick={this.handleClick} className='navbar-brand'>
          <img src={logo} />
        </Link>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto">
            {user.authorized && 
              <NavItem onClick={this.handleClick}>
                <Link className='nav-link' to='profile'>Profile</Link>
              </NavItem>}
            {user.authorized && 
              <NavItem onClick={this.handleClick}>
                <Button color='danger' onClick={this.props.signOut}>Logout</Button>
              </NavItem>}
            {!user.authorized &&
              <NavItem onClick={this.handleClick}>
                <Link className='nav-link' to='login'>Login</Link>
              </NavItem>}
          </Nav>
        </Collapse>
      </Navbar>
    ) 
  }
}

function mapStateToProps({user}) {
  return {user}
};

function mapDispatchToProps(dispatch) {
  return {
    signOut: () => {
      dispatch(signOut());
    }
  }
}
