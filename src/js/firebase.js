import * as firebase from 'firebase';

let config = {
  apiKey: 'AIzaSyCx1xGizGAe7FLPm_7dhW9O4jO-yWsgDMQ',
  authDomain: 'twitter-project-c0405.firebaseapp.com',
  databaseURL: 'https://twitter-project-c0405.firebaseio.com',
  projectId: 'twitter-project-c0405',
};

firebase.initializeApp(config);

export const authRef = firebase.auth();
