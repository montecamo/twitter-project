import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HashRouter, BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { fetchUser, authRequired } from './actions/userActions';

import { history } from './store';

import Home from './components/home.jsx';
import AuthForm from './components/auth-form.jsx';
import Navbar from './components/navbar.jsx';
import Profile from './components/profile.jsx';
import Tweets from './components/tweets.jsx';

const mapStateToProps = ({user}) => {
  return {
    user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkUser: () => {
      dispatch(fetchUser());
    },
    addAuthRequiredPromise: () => {
      dispatch(authRequired());
    }
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Router extends Component { 
  componentDidMount() {
    this.props.checkUser();
  }
  render() {
    let { user } = this.props;
    return (
      <ConnectedRouter history={history}>
        <div className='content'>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" render={(props) => {
              if (user.authorized) {
                return <Redirect to='/' />
              }
              return  <AuthForm {...props} type='login' />
            }} />
            <Route path="/signup" render={(props) => {
              if (user.authorized) {
                return <Redirect to='/' />
              }
              return <AuthForm {...props} type='signup' />
            }} />
            <Route path="/profile" render={(props) => {
              if (!user.authorized) {
                this.props.addAuthRequiredPromise();
                return <Redirect push to='/login' />;
              }
              return  <Profile {...props} />
            }} />
            <Route path="/users/:id" render={(props) => (
              <Tweets {...props} type='custom' />
            )} />
          </Switch>
        </div>
      </ConnectedRouter>
    )
  }
};
